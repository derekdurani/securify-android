﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecurifyAndroid.Modelo
{
    public class Visitante
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public int status { get; set; }
        public int idUsuario { get; set; }
    }
}
