﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecurifyAndroid.Modelo
{
    public class Tipo
    {
        public int id { get; set; }
        public string nombre { get; set; }
    }
}
