﻿using SecurifyAndroid.Controlador;
using SecurifyAndroid.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SecurifyAndroid.Vista
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MiUsuarioPage : ContentPage
    {
        Usuario _usuario = new Usuario();
        string DomicilioCompleto = "";
        public MiUsuarioPage(Usuario usuario, string domicilio)
        {
            _usuario = usuario;
            DomicilioCompleto = domicilio;
            this.Content = interfaces().Content;           
        }

        

        public ContentPage interfaces()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            Label lblheader = new Label
            {
                Text = "Mi Usuario",
                FontSize = 32,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.Start,
                FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Bold.ttf#WorkSans-Bold" : null
            };
            Label lblNombre = new Label
            {
                Text = "Nombre",
                FontSize = 20,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.Fill,
                FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Bold.ttf#WorkSans-Bold" : null
            };

            Label lblNombreU = new Label
            {
                Text = _usuario.nombre,
                FontSize = 16,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.Fill,
                FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Light.ttf#WorkSans-Light" : null
            };
            Label lblDomicilio = new Label
            {
                Text = "Domcilio",
                FontSize = 20,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.Fill,
                FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Bold.ttf#WorkSans-Bold" : null
            };

            Label lblDomicilioU = new Label
            {
                Text = DomicilioCompleto,
                FontSize = 16,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.Fill,
                FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Light.ttf#WorkSans-Light" : null
            };

            Label lblTemporal = new Label
            {
                Text = "Código de entrada temporal",
                FontSize = 20,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.Fill,
                FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Bold.ttf#WorkSans-Bold" : null
            };

            Label lblTemporalU = new Label
            {
                Text = _usuario.temporal,
                FontSize = 14,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.Fill,
                FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Light.ttf#WorkSans-Light" : null
            };

            Label lblEstatus = new Label
            {
                Text = "Estatus",
                FontSize = 20,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.Fill,
                FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Bold.ttf#WorkSans-Bold" : null
            };

            Label lblEstatusU = new Label
            {
                Text = _usuario.status.ToString() == "0"  ? "Bloqueado" : "Pago al corriente",
                FontSize = 16,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.Fill,
                FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Regular.ttf#WorkSans-Regular" : null
            };
            StackLayout stackLayout = new StackLayout
            {
                BackgroundColor = Color.FromHex("212121"),
                Padding = new Thickness(12, 12, 12, 12),
                Children =
                {
                   lblheader,
                   lblNombre,
                   lblNombreU,
                   lblDomicilio,
                   lblDomicilioU,
                   lblTemporal,
                   lblTemporalU,
                   lblEstatus,
                   lblEstatusU
                }
            };

            ContentPage contentPage = new ContentPage();
            contentPage.Content = stackLayout;
            return contentPage;
        }
    }
}