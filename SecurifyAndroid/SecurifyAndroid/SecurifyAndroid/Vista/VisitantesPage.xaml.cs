﻿using SecurifyAndroid.Controlador;
using SecurifyAndroid.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SecurifyAndroid.Vista
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class VisitantesPage : ContentPage
	{
        List<Visitante> visitantes = new List<Visitante>();
        ListView lstVisitantes = new ListView();
        Usuario _usuario = new Usuario();
        public async void getVisitantes()
        {
            VisitanteController visitanteController = new VisitanteController();
            Visitante visitante = new Visitante();
            visitante.idUsuario = _usuario.id;
            visitantes = await visitanteController.GetVisitantesIDUsuario(visitante);
            lstVisitantes.ItemsSource = visitantes;
        }
		public VisitantesPage (Usuario usuario)
		{
            this.Content = interfaces().Content;
            _usuario = usuario;
            getVisitantes();           
        }

        public ContentPage interfaces()
        {            
            NavigationPage.SetHasNavigationBar(this, false);
            Label lblheader = new Label
            {
                Text = "Visitantes",
                FontSize = 32,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.Start,
                FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Bold.ttf#WorkSans-Bold" : null
            };

            Label lblex = new Label()
            {
                Text = "0 = Bloqueado \n1 = Acceso permitido",
                FontSize = 16,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.Start,
                FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Light.ttf#WorkSans-Light" : null
            };

            lstVisitantes = new ListView
            {
                Margin = new Thickness(4, 4, 4, 4),
                IsPullToRefreshEnabled = true,
                RowHeight = 100,
                ItemTemplate = new DataTemplate(() =>
                {
                    Label lblid = new Label();
                    lblid.SetBinding(Label.TextProperty, "id");
                    lblid.FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Medium.ttf#WorkSans-Medium" : null;
                    lblid.TextColor = Color.Transparent;
                    lblid.FontSize = 20;

                    Label lblnombre = new Label();
                    lblnombre.SetBinding(Label.TextProperty, "nombre");
                    lblnombre.FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Medium.ttf#WorkSans-Medium" : null;
                    lblnombre.TextColor = Color.White;
                    lblnombre.FontSize = 20;

                    Label lblstatus = new Label();
                    lblstatus.SetBinding(Label.TextProperty, "status");
                    lblstatus.FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Light.ttf#WorkSans-Light" : null;
                    lblstatus.TextColor = Color.White;
                    lblstatus.FontSize = 15;

                    return new ViewCell
                    {
                        View = new StackLayout
                        {
                            Padding = new Thickness(12, 12, 12, 12),
                            Orientation = StackOrientation.Horizontal,
                            Children =
                                {
                                    new StackLayout
                                    {
                                        Margin = new Thickness(0,0,0,0),
                                        VerticalOptions = LayoutOptions.Center,
                                        Spacing = 2,
                                        WidthRequest = 20,
                                        Children =
                                        {
                                            lblid
                                        }
                                    },
                                    new StackLayout
                                    {
                                         Margin = new Thickness(0,0,0,0),
                                        VerticalOptions = LayoutOptions.Center,
                                        Spacing = 2,
                                        Children =
                                        {
                                            lblnombre,
                                            lblstatus
                                        }
                                    }
                                }
                        }
                    };
                })
            };
            lstVisitantes.RefreshCommand = new Command(() =>
            {
                try
                {
                    lstVisitantes.ItemsSource = null;
                    lstVisitantes.IsRefreshing = true;
                    getVisitantes();
                    lstVisitantes.IsRefreshing = false;
                }
                catch (Exception ex)
                {
                    
                }
            });
            lstVisitantes.ItemSelected += LstVisitantes_ItemSelected;

            StackLayout stackLayout = new StackLayout
            {
                BackgroundColor = Color.FromHex("212121"),
                Padding = new Thickness(12, 12, 12, 12),
                Children =
                {
                   lblheader,
                   lblex,
                   new StackLayout
                   {
                       HeightRequest = 700,
                       Children =
                       {
                           lstVisitantes
                       }
                   }

                }
            };

            ContentPage contentPage = new ContentPage();
            contentPage.Content = stackLayout;
            return contentPage;
        }

        private async void LstVisitantes_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                var visitante = (Visitante)e.SelectedItem;
                if(visitante.status == 0)
                {
                    bool resp = await DisplayAlert("Control de Visitantes", $"¿Desea permitir el acceso al visitante {visitante.nombre}?", "Sí", "No");
                    if (resp)
                    {
                        VisitanteController visitanteController = new VisitanteController();
                        visitante.status = 1;
                        bool res = await visitanteController.BloquarPermitirVisitante(visitante);
                        if(res)
                            DependencyService.Get<IToast>().LongAlert($"El acceso a {visitante.nombre} ha sido concedido");
                        else
                            DependencyService.Get<IToast>().LongAlert("Ha ocurrido un error al intentar conceder el acceso al visitante");
                    }
                    else
                    {
                        VisitanteController visitanteController = new VisitanteController();
                        visitante.status = 0;
                        bool res = await visitanteController.BloquarPermitirVisitante(visitante);
                        if (res)
                            DependencyService.Get<IToast>().LongAlert($"El acceso a {visitante.nombre} ha sido bloqueado");
                        else
                            DependencyService.Get<IToast>().LongAlert("Ha ocurrido un error al intentar bloquear el acceso al visitante");
                    }
                }
                else if(visitante.status == 1)
                {
                    bool resp = await DisplayAlert("Control de Visitantes", $"¿Desea bloquear el acceso al visitante {visitante.nombre}?", "Sí", "No");
                    if (resp)
                    {
                        VisitanteController visitanteController = new VisitanteController();
                        visitante.status = 0;
                        bool res = await visitanteController.BloquarPermitirVisitante(visitante);
                        if (res)
                            DependencyService.Get<IToast>().LongAlert($"El acceso a {visitante.nombre} ha sido bloqueado");
                        else
                            DependencyService.Get<IToast>().LongAlert("Ha ocurrido un error al intentar bloquear el acceso al visitante");
                    }
                    else
                    {
                        VisitanteController visitanteController = new VisitanteController();
                        visitante.status = 1;
                        bool res = await visitanteController.BloquarPermitirVisitante(visitante);
                        if (res)
                            DependencyService.Get<IToast>().LongAlert($"El acceso a {visitante.nombre} ha sido concedido");
                        else
                            DependencyService.Get<IToast>().LongAlert("Ha ocurrido un error al intentar conceder el acceso al visitante");
                    }
                }
            }
            catch (Exception ex)
            {
                
            }
        }
    }
}