﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SecurifyAndroid.Vista
{

    public class Mensaje
    {
        public string nombre { get; set; }
        public string mensaje { get; set; }
        public DateTime fecha { get; set; }
    }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChatPage : ContentPage
    {
        public ChatPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            

            Stack<Mensaje> mensajes = new Stack<Mensaje>();
            for (int i = 0; i < 15; i++)
            {
                Mensaje mensaje = new Mensaje()
                {
                    nombre = $"Derek {i + 1}",
                    mensaje = "Buenas tardes hijo de tu puta madre, vamos a coger alv aquí en la caseta o qué?",
                    fecha = DateTime.Parse(DateTime.Now.ToLongDateString())
                };

                Mensaje mensaje2 = new Mensaje()
                {
                    nombre = $"Guardia {i + 1}",
                    mensaje = "Hello",
                    fecha = DateTime.Parse(DateTime.Now.ToLongDateString())
                };

                mensajes.Push(mensaje);
                mensajes.Push(mensaje2);
            }
            ListView lstmensajes = new ListView
            {
                Margin = new Thickness(4, 4, 4, 4),
                ItemsSource = mensajes,
                IsPullToRefreshEnabled = true,
                RowHeight = 80,
                ItemTemplate = new DataTemplate(() =>
                {
                    Label lblnombre = new Label();
                    lblnombre.SetBinding(Label.TextProperty, "nombre");
                    lblnombre.FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Medium.ttf#WorkSans-Medium" : null;
                    lblnombre.TextColor = Color.White;
                    lblnombre.FontSize = 15;

                    Label lblmensaje = new Label();
                    lblmensaje.SetBinding(Label.TextProperty, "mensaje");
                    lblmensaje.FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Medium.ttf#WorkSans-Medium" : null;
                    lblmensaje.TextColor = Color.White;
                    lblmensaje.FontSize = 13;

                    Label lblfecha = new Label();
                    lblfecha.SetBinding(Label.TextProperty, "fecha");
                    lblfecha.FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Light.ttf#WorkSans-Light" : null;
                    lblfecha.TextColor = Color.White;
                    lblfecha.FontSize = 12;

                    return new ViewCell
                    {
                        View = new StackLayout
                        {
                            Padding = new Thickness(8, 8, 8, 8),
                            Orientation = StackOrientation.Horizontal,
                            Children =
                            {
                                new StackLayout
                                {
                                    HorizontalOptions = LayoutOptions.Center,
                                    VerticalOptions = LayoutOptions.Center,
                                    WidthRequest = 125,
                                    Children =
                                    {
                                        lblnombre
                                    }
                                },
                                new StackLayout
                                {
                                    Margin = new Thickness(0,0,0,0),
                                    VerticalOptions = LayoutOptions.Center,
                                    Spacing = 2,
                                    Children =
                                    {
                                        lblmensaje,
                                        lblfecha
                                    }
                                }
                            }
                        }
                    };
                })
            };

            RoundedEntry txtmensaje = new RoundedEntry
            {
                FontFamily = "WorkSans-Regular.ttf#WorkSans-Regular",
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Placeholder = "Mensaje...",
                WidthRequest = 250,
                Margin = new Thickness(4,4,4,4)
            };

            Button btnenviar = new Button
            {
                Text = "Enviar",
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.EndAndExpand,
                BackgroundColor = Color.FromHex("#000000"),
                TextColor = Color.FromHex("#FFFFFF"),
                FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Medium.ttf#WorkSans-Medium" : null,
                CornerRadius = 20,
                WidthRequest = 80,
                Margin = new Thickness(4, 4, 4, 4)
            };

            StackLayout stackLayout = new StackLayout
            {
                BackgroundColor = Color.FromHex("212121"),
                Padding = new Thickness(4, 4, 4, 4),
                Children =
                {
                    new StackLayout
                    {
                        HeightRequest = 600,
                        Children =
                        {
                            lstmensajes
                        }
                    },
                    
                }
            };

            StackLayout stackLayoutall = new StackLayout
            {
                BackgroundColor = Color.FromHex("212121"),
                Children =
                {
                    stackLayout,
                    new StackLayout
                    {
                        Orientation = StackOrientation.Horizontal,
                        Children =
                        {
                            txtmensaje,
                            btnenviar
                        }
                    }
                }
            };

            this.Content = stackLayoutall;
        }
    }
}