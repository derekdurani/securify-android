﻿using SecurifyAndroid.Controlador;
using SecurifyAndroid.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SecurifyAndroid.Vista
{
    public class VisitasTable
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string fecha { get; set; }
    }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PrincipalPage : ContentPage
    {
        bool firstrun = true;
        Stack<VisitasTable> visitasTables = new Stack<VisitasTable>();
        List<Visita> visitas = new List<Visita>();

        ListView lstVisitas = new ListView();
        Switch @switch = new Switch();
        
        Usuario _usuario = new Usuario();

        public PrincipalPage(Usuario usuario)
        {          
            this.Content = interfaces().Content;
            _usuario = usuario;
            getVisitas();
            putNotificaciones();
        }

        private void putNotificaciones()
        {
            if (!firstrun)
            {
                if (_usuario.notificaciones == 1)
                    @switch.IsToggled = true;
                else
                    @switch.IsToggled = false;
            }
            else
                firstrun = false;
        }

        public ContentPage interfaces()
        {
            NavigationPage.SetHasNavigationBar(this, false);

            Label lblheader = new Label
            {
                Text = "Principal",
                FontSize = 32,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.Start,
                FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Bold.ttf#WorkSans-Bold" : null
            };
            Label lblnotificaciones = new Label
            {
                Text = "Notificaciones",
                FontSize = 16,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.Fill,
                FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Regular.ttf#WorkSans-Regular" : null
            };

            @switch = new Switch
            {
                IsToggled = true,
                OnColor = Color.FromHex("484848"),
                HorizontalOptions = LayoutOptions.Center,
                Scale = 1.5
            };

            @switch.Toggled += switch_Toggled;

            Label lblultimasvisitas = new Label
            {
                Text = "Últimas visitas",
                FontSize = 16,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.Fill,
                FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Regular.ttf#WorkSans-Regular" : null
            };
            lstVisitas = new ListView
            {
                Margin = new Thickness(4, 4, 4, 4),
                ItemsSource = visitasTables,
                IsPullToRefreshEnabled = true,
                RowHeight = 50,
                ItemTemplate = new DataTemplate(() =>
                {

                    Label lblid = new Label();
                    lblid.SetBinding(Label.TextProperty, "id");
                    lblid.FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Medium.ttf#WorkSans-Medium" : null;
                    lblid.TextColor = Color.Transparent;
                    lblid.FontSize = 12;

                    Label lblnombre = new Label();
                    lblnombre.SetBinding(Label.TextProperty, "nombre");
                    lblnombre.FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Medium.ttf#WorkSans-Medium" : null;
                    lblnombre.TextColor = Color.White;
                    lblnombre.FontSize = 15;

                    Label lblfecha = new Label();
                    lblfecha.SetBinding(Label.TextProperty, "fecha");
                    lblfecha.FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Light.ttf#WorkSans-Light" : null;
                    lblfecha.TextColor = Color.White;
                    lblfecha.FontSize = 13;

                    return new ViewCell
                    {
                        View = new StackLayout
                        {
                            Padding = new Thickness(12, 12, 12, 12),
                            Orientation = StackOrientation.Horizontal,
                            Children =
                                {
                                    new StackLayout
                                    {
                                        Margin = new Thickness(0,0,0,0),
                                        VerticalOptions = LayoutOptions.Center,
                                        Children =
                                        {
                                            lblid
                                        }
                                    },
                                    new StackLayout
                                    {
                                        Margin = new Thickness(0,0,0,0),
                                        VerticalOptions = LayoutOptions.Center,
                                        Spacing = 2,
                                        Children =
                                        {
                                            lblnombre,
                                            lblfecha
                                        }
                                    }
                                }
                        }
                    };
                })
            };
            lstVisitas.RefreshCommand = new Command(() => {
                try
                {
                    lstVisitas.ItemsSource = null;
                    visitasTables.Clear();
                    lstVisitas.IsRefreshing = true;
                    getVisitas();
                    lstVisitas.IsRefreshing = false;
                }
                catch (Exception ex)
                {
                    
                }
            });
            Button btnchat = new Button
            {
                Text = "Enviar mensaje al guarida de seguridad",
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center,
                BackgroundColor = Color.FromHex("#000000"),
                TextColor = Color.FromHex("#FFFFFF"),
                FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Medium.ttf#WorkSans-Medium" : null,
                CornerRadius = 20,
                WidthRequest = 200
            };

            btnchat.Clicked += Btnchat_Clicked;

            Button btncamara = new Button
            {
                Text = "Ver cámara de seguridad",
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center,
                BackgroundColor = Color.FromHex("#FFFFFF"),
                TextColor = Color.FromHex("#000000"),
                FontFamily = Device.RuntimePlatform == Device.Android ? "WorkSans-Medium.ttf#WorkSans-Medium" : null,
                CornerRadius = 20,
                WidthRequest = 200
            };

            btncamara.Clicked += Btncamara_Clicked;

            StackLayout stackLayout = new StackLayout
            {
                BackgroundColor = Color.FromHex("212121"),
                Padding = new Thickness(12, 12, 12, 12),
                Children =
                {
                    lblheader,
                    lblnotificaciones,
                    @switch,
                    lblultimasvisitas,
                    new StackLayout
                    {
                        HeightRequest = 400,
                        Children =
                        {
                            lstVisitas
                        }
                    },
                    new StackLayout
                    {
                        HorizontalOptions = LayoutOptions.Fill,
                        Children =
                        {
                            btnchat,
                            btncamara
                        }
                    }

                }
            };

            ContentPage contentPage = new ContentPage();
            contentPage.Content = stackLayout;
            return contentPage;
        }

        private async void switch_Toggled(object sender, ToggledEventArgs e)
        {
            Usuario usuario = new Usuario();
            UsuarioController usuarioController = new UsuarioController();
            if(e.Value == false)
            {
                usuario.id = _usuario.id;
                usuario.notificaciones = 0;
                bool response = await usuarioController.BloquearPermitirNotificaciones(usuario);
                if(response)
                    DependencyService.Get<IToast>().LongAlert("Has bloqueado las notificaciones con éxito");
                else
                    DependencyService.Get<IToast>().LongAlert("Ha ocurrido un error al intentar bloquear las notificaciones");
            }
            else
            {
                usuario.id = _usuario.id;
                usuario.notificaciones = 1;
                bool response = await usuarioController.BloquearPermitirNotificaciones(usuario);
                if (response)
                    DependencyService.Get<IToast>().LongAlert("Has activado las notificaciones con éxito");
                else
                    DependencyService.Get<IToast>().LongAlert("Ha ocurrido un error al intentar activar las notificaciones");
            }
        }

        public async void getVisitas()
        {
            VisitaController visitaController = new VisitaController();           
            Visita visita = new Visita();
            visita.idUsuario = _usuario.id;
            visitas = await visitaController.GetVisitasIDUsuario(visita);
            for (int i = 0; i < visitas.Count; i++)
            {
                VisitanteController visitanteController = new VisitanteController();
                Visitante visitante = new Visitante();
                visitante.id = visitas[i].idVisitante;
                visitante = await visitanteController.GetVisitanteID(visitante);
                VisitasTable visitasTable = new VisitasTable();
                visitasTable.id = visitas[i].id;
                visitasTable.nombre = visitante.nombre;
                visitasTable.fecha = visitas[i].fecha.ToLongDateString();
                visitasTables.Push(visitasTable);
                if (i + 1 >= visitas.Count)
                {
                    lstVisitas.ItemsSource = null;
                    lstVisitas.ItemsSource = visitasTables;
                }
            }

        }

        private async void Btncamara_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new CamaraPage());
        }
        private async void Btnchat_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new ChatPage());
        }
    }
}