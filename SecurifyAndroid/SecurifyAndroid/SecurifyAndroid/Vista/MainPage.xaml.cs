﻿using SecurifyAndroid.Controlador;
using SecurifyAndroid.Modelo;
using SecurifyAndroid.Vista;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace SecurifyAndroid
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private async void OnLogin_Clicked(object sender, EventArgs e)
        {
            UsuarioController usuarioController = new UsuarioController();
            Usuario usuario = new Usuario();
            try
            {
                if (txtCodigo.Text != "" && txtContrasenia.Text != "" && txtCodigo.Text != null && txtContrasenia.Text != null)
                {
                    usuario.codigo = txtCodigo.Text;
                    usuario.password = txtContrasenia.Text;
                    usuario = await usuarioController.LoginUsuario(usuario);
                    if (usuario.id != 0)
                    {
                        if (usuario.idPerfil == 3)
                        {
                            string domicilio = await getDomicilio(usuario);
                            DependencyService.Get<IToast>().LongAlert($"Bienvenido {usuario.nombre}");
                            Navigation.InsertPageBefore(new ContainerPage(usuario,domicilio), this);
                            await Navigation.PopAsync().ConfigureAwait(false);                       
                    }
                        else
                            DependencyService.Get<IToast>().LongAlert("Tu usuario no tiene acceso a este sistema");
                    }
                    else
                        DependencyService.Get<IToast>().LongAlert("Código / Contraseña incorrectos");
                }
                else
                    DependencyService.Get<IToast>().LongAlert("Faltan campos por llenar");
            }
            catch (Exception ex)
            {
                DependencyService.Get<IToast>().LongAlert($"Ha ocurrido un error: {ex.Message}");
            }
        }

        private async Task<string> getDomicilio(Usuario _usuario)
        {
            Usuario usuario = new Usuario();
            UsuarioController usuarioController = new UsuarioController();
            usuario = await usuarioController.GetUsuarioID(_usuario);

            Domicilio domicilio = new Domicilio();
            domicilio.id = usuario.idDomicilio;
            DomicilioController domicilioController = new DomicilioController();
            domicilio = await domicilioController.GetDomicilioID(domicilio);

            Calle calle = new Calle();
            calle.id = domicilio.idCalle;
            CalleController calleController = new CalleController();
            calle = await calleController.GetCalleID(calle);

            Colonia colonia = new Colonia();
            colonia.id = calle.idColonia;
            ColoniaController coloniaController = new ColoniaController();
            colonia = await coloniaController.GetColoniaID(colonia);

            string DomicilioCompleto = $"{calle.nombre} {domicilio.numero} Col. {colonia.nombre}";
            return DomicilioCompleto;
        }
    }
}
