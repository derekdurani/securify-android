﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SecurifyAndroid.Vista
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CamaraPage : ContentPage
	{
		public CamaraPage ()
		{
            NavigationPage.SetHasNavigationBar(this, false);
            WebView webView = new WebView()
            {
                Source = "http://172.18.30.166:5000/api/usuario"
            };

           
            this.Content = webView;
		}
	}
}