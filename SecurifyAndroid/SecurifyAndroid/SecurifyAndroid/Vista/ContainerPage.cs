﻿using SecurifyAndroid.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;

namespace SecurifyAndroid.Vista
{
	public class ContainerPage : Xamarin.Forms.TabbedPage
    {
		public ContainerPage (Usuario usuario, string dom)
		{
            BarBackgroundColor = Color.FromHex("484848");
            BarTextColor = Color.White;

            NavigationPage.SetHasNavigationBar(this, false);
            var principalPage = new NavigationPage(new PrincipalPage(usuario));
            principalPage.Title = "Principal";

            var visitantesPage = new NavigationPage(new VisitantesPage(usuario));
            visitantesPage.Title = "Visitantes";

            var miusuarioPage = new NavigationPage(new MiUsuarioPage(usuario, dom));
            miusuarioPage.Title = "Mi Usuario";

            Children.Add(principalPage);
            Children.Add(visitantesPage);
            Children.Add(miusuarioPage);

            On<Xamarin.Forms.PlatformConfiguration.Android>().SetToolbarPlacement(ToolbarPlacement.Bottom);
        }
	}
}