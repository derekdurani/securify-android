﻿using Newtonsoft.Json;
using SecurifyAndroid.Modelo;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SecurifyAndroid.Controlador
{
    class TipoController
    {
        public async Task<List<Tipo>> GetTipos()
        {
            List<Tipo> _tipos = new List<Tipo>();
            HttpClient client = new HttpClient();
            var response = await client.GetAsync(Helpers.apiUrl + "tipo");
            string jsonString = await response.Content.ReadAsStringAsync();
            return _tipos = JsonConvert.DeserializeObject<List<Tipo>>(jsonString);
        }
    }
}
