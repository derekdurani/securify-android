﻿using Newtonsoft.Json;
using SecurifyAndroid.Modelo;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SecurifyAndroid.Controlador
{
    class ColoniaController
    {
        public async Task<Colonia> GetColoniaID(Colonia colonia)
        {
            Colonia _colonia = new Colonia();
            HttpClient client = new HttpClient();
            var json = JsonConvert.SerializeObject(colonia);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(Helpers.apiUrl + "colonia/getid", content);
            string jsonString = await response.Content.ReadAsStringAsync();
            return _colonia = JsonConvert.DeserializeObject<Colonia>(jsonString);
        }
    }
}
