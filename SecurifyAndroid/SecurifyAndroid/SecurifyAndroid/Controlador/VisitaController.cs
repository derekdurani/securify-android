﻿using Newtonsoft.Json;
using SecurifyAndroid.Modelo;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SecurifyAndroid.Controlador
{
    class VisitaController
    {
        public async Task<List<Visita>> GetVisitasIDUsuario(Visita visita)
        {
            List<Visita> _visitas = new List<Visita>();
            HttpClient client = new HttpClient();
            var json = JsonConvert.SerializeObject(visita);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(Helpers.apiUrl + "visita/getidusuario", content);
            string jsonString = await response.Content.ReadAsStringAsync();
            return _visitas = JsonConvert.DeserializeObject<List<Visita>>(jsonString);
        }
    }
}
