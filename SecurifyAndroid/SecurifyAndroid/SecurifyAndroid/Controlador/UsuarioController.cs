﻿using Newtonsoft.Json;
using SecurifyAndroid.Modelo;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SecurifyAndroid.Controlador
{
    class UsuarioController
    {
        public async Task<Usuario> LoginUsuario(Usuario usuario)
        {
            Usuario _usuario = new Usuario();
            HttpClient client = new HttpClient();
            var json = JsonConvert.SerializeObject(usuario);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(Helpers.apiUrl + "usuario/login", content);
            string jsonString = await response.Content.ReadAsStringAsync();
            return _usuario = JsonConvert.DeserializeObject<Usuario>(jsonString);
        }

        public async Task<List<Usuario>> GetUsuarios()
        {
            List<Usuario> _usuarios = new List<Usuario>();
            HttpClient client = new HttpClient();
            var response = await client.GetAsync(Helpers.apiUrl + "usuario");
            string jsonString = await response.Content.ReadAsStringAsync();
            return _usuarios = JsonConvert.DeserializeObject<List<Usuario>>(jsonString);
        }

        public async Task<Usuario> GetUsuarioID(Usuario usuario)
        {
            Usuario _usuario = new Usuario();
            HttpClient client = new HttpClient();
            var json = JsonConvert.SerializeObject(usuario);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(Helpers.apiUrl + "usuario/getid", content);
            string jsonString = await response.Content.ReadAsStringAsync();
            return _usuario = JsonConvert.DeserializeObject<Usuario>(jsonString);
        }

        public async Task<List<Usuario>> GetUsuariosNombre(Usuario usuario)
        {
            List<Usuario> _usuarios = new List<Usuario>();
            HttpClient client = new HttpClient();
            var json = JsonConvert.SerializeObject(usuario);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(Helpers.apiUrl + "usuario/getnombre", content);
            string jsonString = await response.Content.ReadAsStringAsync();
            return _usuarios = JsonConvert.DeserializeObject<List<Usuario>>(jsonString);
        }

        public async Task<bool> BloquearUsuario(Usuario usuario)
        {
            bool flag;
            HttpClient client = new HttpClient();
            var json = JsonConvert.SerializeObject(usuario);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PutAsync(Helpers.apiUrl + "usuario/bloquear", content);
            string jsonString = await response.Content.ReadAsStringAsync();
            return flag = JsonConvert.DeserializeObject<bool>(jsonString);
        }

        public async Task<bool> BloquearPermitirNotificaciones(Usuario usuario)
        {
            bool flag;
            HttpClient client = new HttpClient();
            var json = JsonConvert.SerializeObject(usuario);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PutAsync(Helpers.apiUrl + "usuario/bloquearpermitirnotificaciones", content);
            string jsonString = await response.Content.ReadAsStringAsync();
            return flag = JsonConvert.DeserializeObject<bool>(jsonString);
        }
    }
}
