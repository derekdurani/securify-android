﻿using Newtonsoft.Json;
using SecurifyAndroid.Modelo;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SecurifyAndroid.Controlador
{
    class CalleController
    {
        public async Task<Calle> GetCalleID(Calle calle)
        {
            Calle _calle = new Calle();
            HttpClient client = new HttpClient();
            var json = JsonConvert.SerializeObject(calle);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(Helpers.apiUrl + "calle/getid", content);
            string jsonString = await response.Content.ReadAsStringAsync();
            return _calle = JsonConvert.DeserializeObject<Calle>(jsonString);
        }
    }
}
