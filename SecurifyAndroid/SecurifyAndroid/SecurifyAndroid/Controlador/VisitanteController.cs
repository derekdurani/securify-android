﻿using Newtonsoft.Json;
using SecurifyAndroid.Modelo;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SecurifyAndroid.Controlador
{
    class VisitanteController
    {
        public async Task<Visitante> GetVisitanteID(Visitante visitante)
        {
            Visitante _visitante = new Visitante();
            HttpClient client = new HttpClient();
            var json = JsonConvert.SerializeObject(visitante);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(Helpers.apiUrl + "visitante/getid", content);
            string jsonString = await response.Content.ReadAsStringAsync();
            return _visitante = JsonConvert.DeserializeObject<Visitante>(jsonString);
        }

        public async Task<List<Visitante>> GetVisitantesIDUsuario(Visitante visitante)
        {
            List<Visitante> _visitantes = new List<Visitante>();
            HttpClient client = new HttpClient();
            var json = JsonConvert.SerializeObject(visitante);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(Helpers.apiUrl + "visitante/getidusuario", content);
            string jsonString = await response.Content.ReadAsStringAsync();
            return _visitantes = JsonConvert.DeserializeObject<List<Visitante>>(jsonString);
        }

        public async Task<bool> BloquarPermitirVisitante(Visitante visitante)
        {
            bool flag = false;
            HttpClient client = new HttpClient();
            var json = JsonConvert.SerializeObject(visitante);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PutAsync(Helpers.apiUrl + "visitante/bloquear", content);
            string jsonString = await response.Content.ReadAsStringAsync();
            return flag = JsonConvert.DeserializeObject<bool>(jsonString);
        }
    }
}
