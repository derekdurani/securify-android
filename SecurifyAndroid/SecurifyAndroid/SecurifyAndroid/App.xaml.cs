﻿using SecurifyAndroid.Vista;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace SecurifyAndroid
{
    public partial class App : Application
    {
        public NavigationPage PrincipalPage { get; }
        public NavigationPage CamaraPage { get; }
        public NavigationPage ChatPage { get; }
        public NavigationPage Visitantes { get; }
        public NavigationPage ContainerPage { get; }
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
