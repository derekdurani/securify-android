﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecurifyAndroid
{
    public interface IToast
    {
        void LongAlert(string message);
        void ShortAlert(string message);
    }
}
